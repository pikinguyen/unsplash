﻿namespace Unsplash.Models.Test
{
    public class UserProfileDto : UnsplashNavigationDto
    {
        public string LikedPhoto { get; set; }
        public string NewUserName { get; set; }
        public string NewFirstName {get; set;}
    }
}
