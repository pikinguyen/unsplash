﻿namespace Unsplash.Models.Test
{
    public class UnsplashNavigationDto
    {
        public string Url { get; set; }
    }
}
