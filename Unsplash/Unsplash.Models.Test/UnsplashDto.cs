﻿namespace Unsplash.Models.Test
{
    public class UnsplashDto
    {
        public LoginDto LoginDto { get; set; }
        public HomePageDto HomePageDto { get; set; }
        public SearchDto SearchDto { get; set; }
    }
}
