﻿namespace Unsplash.Models.Test
{
    public class ImagePopupDto
    {
        public UserPopupDto UserPopupToValidate { get; set; }
        public UserPopupDto UpdatedUserPopupToValidate { get; set; }
    }
}
