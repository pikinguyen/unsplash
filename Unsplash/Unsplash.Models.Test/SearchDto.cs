﻿namespace Unsplash.Models.Test
{
    public class SearchDto
    {
        public string SearchText { get; set; }
    }
}
