﻿namespace Unsplash.Models.Test
{
    public class UserPopupDto
    {
        public string FollowingButtonText { get; set; }
        public string BackgroundColor { get; set; }
    }
}
