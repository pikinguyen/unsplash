﻿namespace Unsplash.Models.Test
{
    public class CollectionDto : UnsplashNavigationDto
    {
        public string CollectionName { get; set; }
        public bool IsPrivateCollection { get; set; }
    }
}
