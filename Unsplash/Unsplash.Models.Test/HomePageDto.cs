﻿namespace Unsplash.Models.Test
{
    public class HomePageDto : UnsplashNavigationDto
    {
        public int ImageIndex { get; set; }
        public ImagePopupDto ImagePopupDto { get; set; }
        public UserProfileDto UserProfileDto { get; set; }
        
    }
}
