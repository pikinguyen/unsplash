﻿using FluentAssertions;
using Unsplash.Test.Business.Pages;

namespace Unsplash.Test.Business.Validations
{
    public interface ILoginPageValidation
    {
        void ValidateLoginPageState();
    }

    internal class LoginPageValidation: ILoginPageValidation
    {
        private readonly ILoginPage _loginPage;

        public LoginPageValidation(ILoginPage loginPage)
        {
            _loginPage = loginPage;
        }

        public void ValidateLoginPageState()
        {
            _loginPage.EmailTextbox.Enabled.Should().BeTrue("Element_NotEnabled", nameof(_loginPage.EmailTextbox));
            _loginPage.PasswordTextbox.Enabled.Should().BeTrue("Element_NotEnabled", nameof(_loginPage.PasswordTextbox));
            _loginPage.LoginButton.Enabled.Should().BeTrue("Element_NotEnabled", nameof(_loginPage.LoginButton));
        }
    }
}
