﻿using FluentAssertions;
using Unsplash.Models.Test;
using Unsplash.Test.Business.Pages;

namespace Unsplash.Test.Business.Validations
{
    public interface IHomePageValidation
    {
        void ValidateDisplayedElement(HomePageDto homePageDto);
    }

    internal class HomePageValidation
    {
        private readonly IHomePage _homePage;

        public HomePageValidation(IHomePage homePage)
        {
            _homePage = homePage;
        }

        public void ValidateDisplayedElement(HomePageDto homePageDto)
        {
            _homePage.Photo(homePageDto.ImageIndex).Displayed.Should().BeTrue("ElementIsNotDisplayed", nameof(_homePage.Photo));
        }
    }
}
