﻿using FluentAssertions;
using System;
using System.IO;
using System.Linq;
using Unsplash.Test.Business.Pages;

namespace Unsplash.Test.Business.Validations
{
    public interface IImagePopupValidation
    {
        void ValidateImagePopupState();
        void ValidateDownloadImageState(string path, string fileName);
    }

    internal class ImagePopupValidation
    {
        private readonly IImagePopupPage _imagePopupPage;

        public ImagePopupValidation(IImagePopupPage imagePopupPage)
        {
            _imagePopupPage = imagePopupPage;
        }

        public void ValidateImagePopupState()
        {
            _imagePopupPage.DownloadButton.Enabled.Should().BeTrue("Element is not enable", nameof(_imagePopupPage.DownloadButton));
        }

        public void ValidateDownloadImageState(string path, string fileName)
        {
            VerifyDownloadState(path, fileName).Should().BeTrue("FileIsNotExisted", fileName);
        }

        private bool VerifyDownloadState(string path, string filename)
        {
            var file = Directory.GetFiles(path).FirstOrDefault(fp => fp.Contains(filename));
            if (file != default)
            {
                var fileInfo = new FileInfo(file);
                var isFresh = DateTime.UtcNow - fileInfo.LastWriteTime < TimeSpan.FromMinutes(1);
                File.Delete(file);
                return isFresh;
            }
            return false;
        }
    }
}
