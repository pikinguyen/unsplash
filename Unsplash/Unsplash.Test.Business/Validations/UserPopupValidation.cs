﻿using FluentAssertions;
using Unsplash.Models.Test;
using Unsplash.Test.Business.Pages;

namespace Unsplash.Test.Business.Validations
{
    public interface IUserPopupValidation
    {
        void ValidateUserPopup(UserPopupDto userPopupDto);
    }

    internal class UserPopupValidation
    {
        private readonly IUserPopupPage _userPopupPage;
        public UserPopupValidation(IUserPopupPage userPopupPage)
        {
            _userPopupPage = userPopupPage;
        }

        public void ValidateUserPopup(UserPopupDto userPopupDto)
        {
            _userPopupPage.FollowButton.Enabled.Should().BeTrue(
                "Element is not enabled", 
                nameof(_userPopupPage.FollowButton));

            var backgroundColor = _userPopupPage.FollowButton.GetCssValue("background-color");

            backgroundColor.Should().Be(
                userPopupDto.BackgroundColor,
                "Element_HasExpectedValueInsteadOfActualValue",
                nameof(_userPopupPage.FollowButton), userPopupDto.BackgroundColor, backgroundColor);
        }
    }
}
