﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using Unsplash.Models.Test;
using Unsplash.Test.Business.Pages;

namespace Unsplash.Test.Business.Validations
{
    public interface IUserProfileValidation
    {
        void ValidateUserProfileData(UserProfileDto userProfileDto);
        void ValidateFirstName(string newFirstName);
    }

    internal class UserProfileValidation: IUserProfileValidation
    {
        private readonly IUserProfilePage _userProfilePage;

        public UserProfileValidation(IUserProfilePage userProfilePage)
        {
            _userProfilePage = userProfilePage;
        }

        public void ValidateUserProfileData(UserProfileDto userProfileDto)
        {
            _userProfilePage.LikeNumberLabel.Displayed.Should().BeTrue("ElementIsNotDisplayed",
                nameof(_userProfilePage.LikeNumberLabel));

            _userProfilePage.LikeNumberLabel.Text.Should().Be(userProfileDto.LikedPhoto,
                "Element_HasExpectedValueInsteadOfActualValue",
                nameof(_userProfilePage.LikeNumberLabel),
                userProfileDto.LikedPhoto, _userProfilePage.LikeNumberLabel.Text);

            _userProfilePage.LikedPhoto.Should().Be(userProfileDto.LikedPhoto,
                "Element_HasExpectedValueInsteadOfActualValue",
                nameof(_userProfilePage.LikedPhoto),
                userProfileDto.LikedPhoto, _userProfilePage.LikedPhoto);
        }

        public void ValidateFirstName(string newFirstName)
        {
            string actualValue = _userProfilePage.NameLable.Split(' ')[0];
            actualValue.Should().Be(newFirstName, "SearchResultTitleMatched", actualValue, newFirstName);
        }
    }
}
