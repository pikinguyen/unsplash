﻿using System;
using System.Collections.Generic;
using System.Text;
using Unsplash.Test.Business.Pages;

namespace Unsplash.Test.Business.Validations
{
    public interface IEditProfileValidation
    {

    }

    internal class EditProfileValidation: IEditProfileValidation
    {
        private readonly IEditProfilePage _editProfilePage;  

        public EditProfileValidation(IEditProfilePage editProfilePage)
        {
            _editProfilePage = editProfilePage;
        }
    }
}
