﻿using OpenQA.Selenium;
using Unsplash.Models.Test;
using Unsplash.Test.Business.Pages;

namespace Unsplash.Test.Business.Managers
{
    public interface IUserProfileManager
    {

    }

    internal class UserProfileManager: IUserProfileManager
    {
        private readonly IUserProfilePage _userProfilePage;
        private readonly EditProfilePage _editProfilePage;

        public UserProfileManager(
            IUserProfilePage userPopupPage,
            EditProfilePage editProfilePage)
        {
            _userProfilePage = userPopupPage;
            _editProfilePage = editProfilePage;
        }
        public void OpenEditProfilePage()
        {
            _userProfilePage.EditProfileButton.Click();
        }
        public void OpenAccountSettingPage()
        {
            //Todo: Using Wait
            _userProfilePage.AvatarButton.Click();
            _userProfilePage.SetImpliciteTimeout(2);
            _userProfilePage.AccountSettingButton.Click();
        }
        public void OpenViewProfilePage()
        {
            _userProfilePage.AvatarButton.Click();
            _userProfilePage.SetImpliciteTimeout(2);
            _userProfilePage.ViewProfileButton.Click();
        }

        public void RefreshPage()
        {
            _userProfilePage.RefreshPage();
        }

        public bool OpenCollectionTab()
        {
            if (!_userProfilePage.CollectionButton.Displayed)
            {
                return false;
            }

            _userProfilePage.CollectionButton.Click();
            return true;
        }

        public void UpdateUserProfile(UserProfileDto userProfileDto)
        {
            //Todo: Wrong command action
            if (userProfileDto.NewUserName != default)
            {
                _editProfilePage.Action.SendKeys(Keys.Control + "a");
                _editProfilePage.Action.SendKeys(Keys.Delete);
                _editProfilePage.UsernameTextbox.SendKeys(userProfileDto.NewUserName);
            }

            if (userProfileDto.NewFirstName != null)
            {
                _editProfilePage.FirstnameTextbox.Clear();
                _editProfilePage.FirstnameTextbox.SendKeys(userProfileDto.NewFirstName);
            }

            _editProfilePage.UpdateAccountButton.Click();
        }

        public void NavigateBackToProfilePage(UserProfileDto userProfileDto)
        {
            _editProfilePage.WebDriver.Navigate().GoToUrl(userProfileDto.Url);
        }
    }
}
