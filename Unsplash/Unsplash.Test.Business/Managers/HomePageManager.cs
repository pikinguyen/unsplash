﻿using System;
using System.Collections.Generic;
using System.Text;
using Unsplash.Models.Test;
using Unsplash.Test.Business.Pages;
using Unsplash.Test.Business.Validations;

namespace Unsplash.Test.Business.Managers
{
    internal class HomePageManager
    {
        private readonly IHomePage _homePage;
        private readonly IUserProfilePage _userProfilePage;
        private readonly IEditProfilePage _editProfilePage;
        private readonly IHomePageValidation _homePageValidation;

        public HomePageManager(
            IHomePage homePage,
            IUserProfilePage userProfilePage,
            IEditProfilePage editProfilePage,
            IHomePageValidation homePageValidation)
        {
            _homePage = homePage;
            _userProfilePage = userProfilePage;
            _editProfilePage = editProfilePage;
            _homePageValidation = homePageValidation;
        }

        public void OpenLoginPage()
        {
            _homePage.LoginButton.Click();
        }

        public void SelectImage(HomePageDto homePageDto)
        {
            _homePageValidation.ValidateDisplayedElement(homePageDto);
            _homePage.Photo(homePageDto.ImageIndex).Click();
        }

        public void LikeImage(HomePageDto homePageDto)
        {
            var randomIndexList = new List<int>();
            var randomIndex = new Random();
            homePageDto.ImageIndex = randomIndex.Next(0, 10);
            if (!randomIndexList.Contains(homePageDto.ImageIndex))
            {
                randomIndexList.Add(homePageDto.ImageIndex);
                _homePage.GetAction().MoveToElement(_homePage.Photo(homePageDto.ImageIndex));
                _homePage.LikeButton(homePageDto.ImageIndex).Click();
            }
        }

        public void NavigateToUserProfilePage(HomePageDto homePageDto)
        {
            _homePage.NavigateToPage(homePageDto.Url);
        }

        public void OpenEditProfilePage()
        {
            _userProfilePage.EditProfileButton.Click();
        }
        public void OpenAccountSettingPage()
        {
            //Todo: Using Wait
            _userProfilePage.AvatarButton.Click();
            _userProfilePage.SetImpliciteTimeout(2);
            _userProfilePage.AccountSettingButton.Click();
        }
        public void OpenViewProfilePage()
        {
            _userProfilePage.AvatarButton.Click();
            _userProfilePage.SetImpliciteTimeout(2);
            _userProfilePage.ViewProfileButton.Click();
        }
    }
}
