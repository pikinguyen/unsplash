﻿using System;
using System.Collections.Generic;
using System.Text;
using Unsplash.Models.Test;
using Unsplash.Test.Business.Pages;

namespace Unsplash.Test.Business.Managers
{
    public interface ILoginManager
    {
        void LoginByDefault(LoginDto loginDto);
    }

    internal class LoginManager: ILoginManager
    {
        private readonly ILoginPage _loginPage;

        public LoginManager(ILoginPage loginPage)
        {
            _loginPage = loginPage;
        }

        public void LoginByDefault(LoginDto loginDto)
        {
            if (loginDto.Username != null)
            {
                _loginPage.EmailTextbox.SendKeys(loginDto.Username);
            }
            if (loginDto.Password != null)
            {
                _loginPage.PasswordTextbox.SendKeys(loginDto.Password);
            }

            _loginPage.LoginButton.Click();
        }
    }
}
