﻿using Unsplash.Models.Test;
using Unsplash.Test.Business.Pages;
using Unsplash.Test.Business.Validations;

namespace Unsplash.Test.Business.Managers
{
    public interface IImagePopupManager
    {
        void FollowUser(ImagePopupDto imagePopupDto);
        void DownloadImage(string filename);
    }

    internal class ImagePopupManager
    {
        private const string DownloadPath = "C:/Users/Admin/Downloads";
        private readonly IImagePopupPage _imagePopupPage;
        private readonly IUserPopupPage _userPopupPage;
        private readonly IImagePopupValidation _imagePopupValidation;
        private readonly IUserPopupValidation _userPopupValidation;

        public ImagePopupManager(
            IImagePopupPage imagePopupPage,
            IUserPopupPage userPopupPage,
            IImagePopupValidation imagePopupValidation,
            IUserPopupValidation userPopupValidation)
        {
            _imagePopupPage = imagePopupPage;
            _userPopupPage = userPopupPage;
            _userPopupValidation = userPopupValidation;
            _imagePopupValidation = imagePopupValidation;
        }
        
        public void FollowUser(ImagePopupDto imagePopupDto)
        {
            _imagePopupPage.MoveToElement(_imagePopupPage.FirstPhotoUser);
            _userPopupValidation.ValidateUserPopup(imagePopupDto.UserPopupToValidate);
            _userPopupPage.FollowButton.Click();
            _userPopupValidation.ValidateUserPopup(imagePopupDto.UpdatedUserPopupToValidate);
        }

        public void DownloadImage(string filename)
        {
            _imagePopupPage.DownloadButton.Click();
            _imagePopupValidation.ValidateDownloadImageState(DownloadPath, filename);
        }
    }
}
