﻿using OpenQA.Selenium;

namespace Unsplash.Test.Business.Pages
{
    public interface IUnsplashAddImagePopupPage
    {
        public IWebElement NewCollectionButton { get; }
        public IWebElement CollectionNameTextbox { get; }
        public IWebElement PrivateCheckbox { get; }
        public IWebElement CreateCollectionButton { get; }
        public int CollectionNumber { get; }
        public IWebElement Collection(int collectionIndex);
        public IWebElement AddImageButton(int collectionIndex);
    }

    internal class UnsplashAddImagePopupPage : GeneralPage
    {
        public UnsplashAddImagePopupPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement NewCollectionButton => WebDriver.FindElements(By.CssSelector("button._3w-Ag"))[0];
        public IWebElement CollectionNameTextbox => WebDriver.FindElements(By.CssSelector("input._1qLXi"))[0];
        public IWebElement PrivateCheckbox => WebDriver.FindElements(By.CssSelector("input.WPrwJ"))[0];
        public IWebElement CreateCollectionButton => WebDriver.FindElements(By.CssSelector("button.BUaxM"))[0];
        public IWebElement Collection(int collectionIndex) => WebDriver.FindElements(By.CssSelector("button._1H-jx"))[collectionIndex];
        public int CollectionNumber => WebDriver.FindElements(By.CssSelector("button._1H-jx")).Count;
        public IWebElement AddImageButton(int collectionIndex) => WebDriver.FindElements(By.CssSelector("svg._1hUf9"))[collectionIndex];
    }
}
