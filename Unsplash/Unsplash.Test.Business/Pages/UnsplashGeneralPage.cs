﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Unsplash.Test.Business.Pages
{
    public abstract class GeneralPage
    {
        public IWebDriver WebDriver;
        public Actions Action;

        public GeneralPage(IWebDriver webDriver)
        {
            WebDriver = webDriver;
            Action = new Actions(webDriver);
        }
    }
}
