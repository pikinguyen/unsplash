﻿using OpenQA.Selenium;

namespace Unsplash.Test.Business.Pages
{
    public interface ILoginPage
    {
        public IWebElement EmailTextbox { get; }
        public IWebElement PasswordTextbox { get; }
        public IWebElement LoginButton { get; }
    }

    internal class LoginPage : GeneralPage, ILoginPage
    {
        public LoginPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement EmailTextbox => WebDriver.FindElements(By.CssSelector("#user_email"))[0];
        public IWebElement PasswordTextbox => WebDriver.FindElements(By.CssSelector("#user_password"))[0];
        public IWebElement LoginButton => WebDriver.FindElements(By.CssSelector("input.btn"))[0];
    }
}
