﻿using OpenQA.Selenium;

namespace Unsplash.Test.Business.Pages
{
    public interface IUnsplashSearchPhotoResultPage
    {
        string GetPageHeaderTitle();
    }

    internal class UnsplashSearchPhotoResultPage : GeneralPage, IUnsplashSearchPhotoResultPage
    {
        public UnsplashSearchPhotoResultPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        private IWebElement PageHeaderTitleLabel => WebDriver.FindElement(By.ClassName("_1GUcz"));

        public string GetPageHeaderTitle()
        {
            return this.PageHeaderTitleLabel.Text;
        }
    }
}
