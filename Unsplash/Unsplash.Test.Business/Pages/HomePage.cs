﻿using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Unsplash.Test.Business.Pages
{
    public interface IHomePage
    {
        public IWebElement LoginButton { get; }
        public IWebElement SearchBox { get; }
        public IWebElement LikeButton(int imageIndex);
        public IWebElement AddButton(int imageIndex);
        public IWebElement Photo(int imageIndex);
        public string PhotoName(int imageIndex);
        void SearchByText(string searchKey);
        public Actions GetAction();
        void NavigateToPage(string url);
    }

    internal class HomePage : GeneralPage, IHomePage
    {
        public HomePage(IWebDriver webDriver) : base(webDriver) 
        {
        }

        public IWebElement LoginButton => WebDriver.FindElements(By.CssSelector("a._1hjZT"))[0];
        
        public IWebElement SearchBox => WebDriver.FindElement(By.Name("searchKeyword"));

        public IWebElement LikeButton(int imageIndex) => WebDriver.FindElements(By.CssSelector("button._1EJJ-"))[imageIndex];
        public IWebElement AddButton(int imageIndex) => WebDriver.FindElements(By.CssSelector("button._3dBbn"))[imageIndex];
        public IWebElement Photo(int imageIndex) => WebDriver.FindElements(By.CssSelector("img._2UpQX"))[imageIndex];
        public string PhotoName(int imageIndex) => WebDriver.FindElements(By.CssSelector("a._2Mc8_"))[imageIndex + 1].GetAttribute("href").Split().Last();

        public void SearchByText(string searchKey)
        {
            this.SearchBox.Clear();
            this.SearchBox.SendKeys(searchKey + Keys.Enter);
        }

        public Actions GetAction()
        {
            return Action;
        }

        public void NavigateToPage(string url)
        {
            WebDriver.Navigate().GoToUrl(url);
        }
    }
}
