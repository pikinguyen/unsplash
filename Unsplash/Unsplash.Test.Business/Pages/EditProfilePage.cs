﻿using OpenQA.Selenium;

namespace Unsplash.Test.Business.Pages
{
    public interface IEditProfilePage
    {
        public IWebElement UsernameTextbox { get; }
        public IWebElement UpdateAccountButton { get; }
        public IWebElement FirstnameTextbox { get; }
    }

    internal class EditProfilePage : GeneralPage, IEditProfilePage
    {
        public EditProfilePage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement UsernameTextbox => WebDriver.FindElements(By.CssSelector("input[name='user[username]']"))[0];
        public IWebElement UpdateAccountButton => WebDriver.FindElements(By.CssSelector("input.btn"))[0];
        public IWebElement FirstnameTextbox => WebDriver.FindElements(By.CssSelector("input[name='user[first_name]']"))[0];
    }
}
