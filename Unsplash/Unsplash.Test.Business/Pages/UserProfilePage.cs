﻿using OpenQA.Selenium;
using System;

namespace Unsplash.Test.Business.Pages
{
    public interface IUserProfilePage
    {
        public IWebElement EditProfileButton { get; }
        public IWebElement LikeNumberLabel { get; }
        public string LikedPhoto { get; }
        public IWebElement CollectionButton { get; }
        public IWebElement AvatarButton { get; }
        public IWebElement AccountSettingButton { get; }
        public IWebElement ViewProfileButton { get; }
        public string NameLable { get; }
        public void RefreshPage();
        void SetImpliciteTimeout(double minutes);
    }

    internal class UserProfilePage : GeneralPage, IUserProfilePage
    {
        public UserProfilePage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement EditProfileButton => WebDriver.FindElements(By.CssSelector("a._1cvXQ"))[0];
        public IWebElement LikeNumberLabel => WebDriver.FindElements(By.CssSelector("span._3ruL8"))[2];
        public string LikedPhoto => WebDriver.FindElements(By.CssSelector("img._2UpQX")).Count.ToString();
        public IWebElement CollectionButton => WebDriver.FindElements(By.CssSelector("a.qvEaq"))[2];
        public IWebElement AvatarButton => WebDriver.FindElements(By.Id("popover-avatar-loggedin-menu-desktop"))[0];
        public IWebElement AccountSettingButton => WebDriver.FindElement(By.XPath("//*[@class='_2HZSj _2_izy _16e5w _2eavX _2YS8p']/ul/li[3]/a"));
        public IWebElement ViewProfileButton => WebDriver.FindElement(By.XPath("//*[@class='_2HZSj _2_izy _16e5w _2eavX _2YS8p']/ul/li[1]/a"));
        public string NameLable => WebDriver.FindElements(By.CssSelector("div._2227S > div"))[0].Text;

        public void RefreshPage()
        {
            WebDriver.Navigate().Refresh();
        }

        public void SetImpliciteTimeout(double minutes)
        {
            WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(minutes);
        }
    }
}
