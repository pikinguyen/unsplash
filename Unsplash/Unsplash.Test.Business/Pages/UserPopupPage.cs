﻿using OpenQA.Selenium;

namespace Unsplash.Test.Business.Pages
{
    public interface IUserPopupPage
    {
        IWebElement FollowButton { get; }
    }

    internal class UserPopupPage : GeneralPage, IUserPopupPage
    {
        public UserPopupPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement FollowButton => WebDriver.FindElements(By.CssSelector("button.BUaxM"))[0];
    }
}
