﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Unsplash.Test.Business.Pages
{
    public interface IImagePopupPage
    {
        public IWebElement FirstPhotoUser { get; }
        public IWebElement DownloadButton { get; }
        public IWebElement LikeButton(int buttonIndex);
        public void MoveToElement(IWebElement element);
    }

    internal class ImagePopupPage : GeneralPage, IImagePopupPage
    {
        public ImagePopupPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement FirstPhotoUser => WebDriver.FindElements(By.CssSelector("._2iWc- > span > div._2BNtU > span > div > a > div > div > img"))[0];
        public IWebElement DownloadButton => WebDriver.FindElements(By.CssSelector("span._2Aga-"))[0];
        public IWebElement LikeButton(int buttonIndex) => WebDriver.FindElements(By.CssSelector("button[title='Like photo']"))[buttonIndex];
        public void MoveToElement(IWebElement element)
        {
            Action.MoveToElement(element).Perform();
        }
    }
}
